console.log('this global >>', this);

function Students() {
    // console.log('this inside function ', this);
    // this.school = 'brodway';
    // this.address = 'tinkune';
    // this.getAddr = function() {
    //         return this.address;
    //     }
    // this here is constructor where initial value are initialized
    // constructor block
}
Students.prototype.name = 'brodway infosys nepal';
Students.prototype.country = 'nepal';
Students.prototype.getAddress = function() {
    return this.country;
}

var ram = new Students();
console.log('ram is >>', ram.name);

var shyam = new Students();
console.log('shyam is >>', shyam.country);

var gita = new Students();
console.log('gita is >>', gita.getAddress());

// functional constructor is basics of prototype based OOP
// functional constructor is function having following properties

// functional constructor will never returns
// when calling a functional construcotr we use new keyword
Array.prototype.kishor = function() {
    return 'testing prototype';
}
var arr = new Array()


console.log(arr.length)
arr.push('hi');
console.log('arr', arr.kishor());

var obj = new Object();

var str = new String()

var a = new Boolean()

var c = new Number();