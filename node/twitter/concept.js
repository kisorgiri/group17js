// nodejs is run time environment for javascript on server

// npm // npm is development tool used for nodejs application

// package.json
 // this is javascript project introductory file which contains meaningful information of project and try to keep information up to date

// package-lock.json // this file will lock the current installed version of dependencies

// node_modules // every pacakges installed from npmjs.com are stored in node_modules folder
// never modify any code of node_modules

//command
// npm init // initiliaze any js project with basic inputs and create package.json file

// npm install <pacakge_name>  this will install package from global repo to local repo and update package.json file
// npm install // this will install all the dependencies that are listed in package.json file
// npm uninstall <package_name> uninstall paackage from node_modules and update pacakge.json file