// var a = 'brodway';
// var b = a;

// // a = 'infosys nepal';
// b= 'tinkune';

// console.log('a is >>', a);
// console.log('b is >>', b);
// string,number,boolean(primitive data type ) are immutable properties
// immutable properties ==> if original value is changed it is not reflected in refrence and vice versa


var fruits = ['apple', 'banana'];
var vegitables = fruits;

// fruits[0] ='pineapple';
vegitables[1] = 'ram';

console.log('fruits >>', fruits);
console.log('vegitables >>', vegitables);

// mutable properties ==> if original value is changed it is reflected in refrence and vice versa
// array and object are mutable properties in javascript


function sendMail(details) {
	details.name = 'shyam';

}
var ab = {
    name: 'ram',
    place: 'tinkune',
    phone: '333'
}
sendMail(ab)
console.log('ab >>>',ab);

function groupBy(arr) {

}